class DiskPhone: CallReceiver {
    override val vendor: String = "Tesla"

    override fun call(number: String) {
        println("Disk phone is calling $number")
    }
}
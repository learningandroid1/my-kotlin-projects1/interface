class MobilePhone: CallReceiver, WebBrowser {
    override val vendor: String = "Samsung"

    override fun call(number: String) {
        println("Mobile phone is calling $number")
    }

    override fun playVideo(url: String) {
        println("Mobile phone is playing video from $url")
    }

    override fun receiveCall(number: String) {
        super.receiveCall(number)
        println("LED on")
    }
}
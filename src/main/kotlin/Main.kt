fun main() {

    val devices = listOf(
        AppleWatch(),
        SmartFridge(),
        DiskPhone(),
        RadioPhone(),
        MobilePhone()
    )

    devices.forEach { it.call("8800") }

    val mobilePhone = MobilePhone()
    mobilePhone.apply {
        call("8126381219")
        playVideo("qhwehqwekq")
    }

    val webWatchers = listOf(MobilePhone(), SmartTV())
    webWatchers.forEach {
        it.playVideo("www.examples.com")
        it.call("81273614212")
    }

    val anonymous = object: Callable {
        val name = "anonymous"
        val lastName = "object"

        fun method() = println("anonymous object")

        override val vendor: String = "anonymous vendor"

        override fun call(number: String) {
            println("anonymous object is calling")
        }
    }

    anonymous.name
    anonymous.lastName
    anonymous.method()

    val test = AnonymousTest()
    test.testMethod(anonymous)
}

class AnonymousTest{
    private val anonymous = object: Callable {
        val name = "anonymous"
        val lastName = "object"

        fun method() = println("anonymous object")

        override val vendor: String = "anonymous vendor"

        override fun call(number: String) {
            println("anonymous object is calling")
        }
    }

    fun testMethod(obj: Callable){
        obj.vendor
    }
}

class SmartTV: Callable, WebBrowser {
    override val vendor: String = "Xiaomi"

    override fun call(number: String) {
        println("SmartTV is calling $number")
    }

    override fun playVideo(url: String) {
        println("SmartTV is playing video from $url")
    }
}
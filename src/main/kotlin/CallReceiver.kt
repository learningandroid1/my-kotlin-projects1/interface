interface CallReceiver: Callable {
    fun receiveCall(number: String){
        println("$vendor receives call from $number")
    }
}
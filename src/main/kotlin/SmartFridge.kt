class SmartFridge: Callable {
    override val vendor: String = "LG"

    override fun call(number: String) {
        println("Fridge is calling $number")
    }
}
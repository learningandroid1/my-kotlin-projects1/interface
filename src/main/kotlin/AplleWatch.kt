class AppleWatch: Callable {
    override val vendor: String = "Apple"

    override fun call(number: String) {
        println("Smart watches is calling $number")
    }
}
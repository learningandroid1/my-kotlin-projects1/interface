class RadioPhone: CallReceiver {
    override val vendor: String = "Panasonic"

    override fun call(number: String) {
        println("Radio phone is calling $number")
    }
}
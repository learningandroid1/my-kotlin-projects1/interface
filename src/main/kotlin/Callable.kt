interface Callable {
    val vendor: String
    fun call(number: String)
}
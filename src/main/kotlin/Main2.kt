fun main() {
    val evenChecker = object : IntPredicate {
        override fun accept(i: Int): Boolean {
            return i%2 == 0
        }
    }

    println("9 is even - ${evenChecker.accept(9)}")
    println("8 is even - ${evenChecker.accept(8)}")
}

fun interface IntPredicate{
    fun accept(i: Int): Boolean
}